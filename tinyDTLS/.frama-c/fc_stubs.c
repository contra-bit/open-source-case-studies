#include "alert.h"
#include "sha2/sha2.h"
#include  "aes/rijndael.h"
#include "ecc/ecc.h"
#include "ccm.h"
#include "crypto.h"
#include "dtls_config.h"
#include "dtls_debug.h"
#include "dtls.h"
#include "dtls_mutex.h"
#include "dtls_prng.h"
#include "dtls_time.h"
#include "global.h"
#include "hmac.h"
#include "netq.h"
#include "numeric.h"
#include "peer.h"
#include "session.h"
#include "state.h"
#include "tinydtls.h"
#include "utlist.h"
#include "__fc_builtin.h"


// Andre's WIP stubs
ssize_t sendto(int sockfd, const void *buf, size_t len, int flags,
               const struct sockaddr *address, socklen_t address_len) {
  // assuming preconditions already checked
  if (Frama_C_nondet(0, 1)) {
    // error
    int possible_errors[] = {
      EACCES,
      EWOULDBLOCK, // and many others
    };
    errno = possible_errors[Frama_C_interval(0, sizeof(possible_errors)/sizeof(int)-1)];
    return -1;
  } else {
    // non-error
    return Frama_C_long_interval(0, len);
  }
}

ssize_t recvfrom(int sockfd, void *buf, size_t len, int flags,
                 struct sockaddr *addrbuf, socklen_t *addrbuf_len) {
  // assuming preconditions already checked
  if (Frama_C_nondet(0, 1)) {
    // error
    int possible_errors[] = {
      EAGAIN,
      EWOULDBLOCK, // and many others
    };
    errno = possible_errors[Frama_C_interval(0, sizeof(possible_errors)/sizeof(int)-1)];
    return -1;
  } else {
    // non-error
    ((char*)buf)[Frama_C_interval(0, len-1)] = Frama_C_interval(0, 255);
    if (addrbuf) {
      // TODO: write source address data
      //addrbuf = ...
      //addrbuf_len = ...
    }
    return Frama_C_long_interval(0, len);
  }

}
